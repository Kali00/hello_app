class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

	def hello
		render html: "Hola, mundo! What can you tell me about this very first app?!"
	end

end
